const icons = {
    copy: require('../assets/svg/copy.vue')
};

const BaseIcon = {
    props: {
        name: {
            required: true,
            type: String,
            validator(value) {
                return Object.prototype.hasOwnProperty.call(icons, value)
            }
        }
    },

    data() {
        return {
            selectedTab: null,

            misc: {
                autoRepeaters: false,
                blockAssist: false
            }
        };
    },

    computed: {
        iconComponent() {
          return icons[this.name]
        }
    },

    template: `
        <component
            :is="iconComponent"
            role="img"
            style="height: 1em; width: 1em;"
        />
    `
};

export default BaseIcon;