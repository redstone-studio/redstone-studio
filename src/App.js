import { computed, ref, reactive, watch } from 'vue';
import { createApp } from 'vue';
import { ElCheckbox, ElInput, ElDialog } from 'element-plus';

const generateId = () => Math.random().toString(36).toUpperCase().substring(2, 6);

const App = {};

App.self = {
    setup() {
        const selectedTab = ref('');
        const misc = reactive({
            autoRepeaters: false,
            blockAssist: false
        });
        
        const emptyProjectId = generateId();
        const projects = ref([
            {
                id: emptyProjectId,

                data: {
                    tickSpeed: 10
                }
            }
        ]);

        const currentProject = ref(projects.value.find(i => i.id === emptyProjectId));

        const copySelection = () => {
            console.log('copy selection');
        };

        const pasteSelection = () => {
            console.log('paste selection');
        };

        const startSimulation = () => {
            console.log('start simulation');
        };

        const resetSimulation = () => {
            console.log('reset simulation');
        };

        const manualTick = () => {
            console.log('manual tick');
        };

        const switchProject = (id) => {
            currentProject.value = projects.value.find(i => i.id === id);
        };

        const addProject = () => {
            const newProjectId = generateId();

            projects.value.push({
                id: newProjectId,
                data: {
                    tickSpeed: 10
                }
            });

            switchProject(newProjectId);
        };

        const removeProject = (idx) => {
            if (confirm(`Delete project with id ${projects.value[idx].id}?`));
            switchProject(projects.value[idx === 0 ? 1 : idx - 1].id);
            projects.value.splice(idx, 1);
        };

        return {
            projects,
            currentProject,

            selectedTab,

            misc,

            copySelection,
            pasteSelection,
            startSimulation,
            resetSimulation,
            manualTick,

            addProject,
            switchProject,
            removeProject
        };
    }
};

App.init = function initVue(root) {
    const app = createApp(root);

    app.component('BaseCheckbox', ElCheckbox);
    app.component('BaseInput', ElInput);
    app.component('BaseInput', ElDialog);


    const vm = app.mount('.app-container');

    return vm;
}


export default App;
