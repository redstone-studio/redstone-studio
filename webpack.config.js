const path = require('path');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
    },
    mode: 'development',

    resolve: {
        alias: {
          'vue': 'vue/dist/vue.esm-bundler.js'
        }
    },

    plugins: [
        new BrowserSyncPlugin({
            host: 'localhost',
            port: 3000,
            server: { baseDir: ['./'] }
        })
    ],

    module: {
        rules: [
          {
            test: /\.svg$/,
            use: [
              'vue-loader',
              'vue-svg-loader',
            ]
          }
        ]
    }
};